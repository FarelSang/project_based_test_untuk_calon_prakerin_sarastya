﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Label1 = New Label()
        Label2 = New Label()
        txtusername2 = New TextBox()
        Label3 = New Label()
        txtpassword2 = New TextBox()
        btnlogin = New Button()
        btnback = New Button()
        SuspendLayout()
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Font = New Font("Stencil", 19.8000011F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        Label1.Location = New Point(137, 24)
        Label1.Name = "Label1"
        Label1.Size = New Size(215, 40)
        Label1.TabIndex = 1
        Label1.Text = "Form Login"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label2.Location = New Point(72, 114)
        Label2.Name = "Label2"
        Label2.Size = New Size(104, 28)
        Label2.TabIndex = 2
        Label2.Text = "Username"
        ' 
        ' txtusername2
        ' 
        txtusername2.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtusername2.Location = New Point(72, 150)
        txtusername2.Name = "txtusername2"
        txtusername2.Size = New Size(351, 34)
        txtusername2.TabIndex = 3
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label3.Location = New Point(76, 216)
        Label3.Name = "Label3"
        Label3.Size = New Size(44, 28)
        Label3.TabIndex = 4
        Label3.Text = "NIS"
        ' 
        ' txtpassword2
        ' 
        txtpassword2.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtpassword2.Location = New Point(76, 253)
        txtpassword2.MaxLength = 5
        txtpassword2.Name = "txtpassword2"
        txtpassword2.PasswordChar = "*"c
        txtpassword2.Size = New Size(347, 34)
        txtpassword2.TabIndex = 5
        ' 
        ' btnlogin
        ' 
        btnlogin.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        btnlogin.Location = New Point(197, 368)
        btnlogin.Name = "btnlogin"
        btnlogin.Size = New Size(94, 41)
        btnlogin.TabIndex = 6
        btnlogin.Text = "Login"
        btnlogin.UseVisualStyleBackColor = True
        ' 
        ' btnback
        ' 
        btnback.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        btnback.Location = New Point(193, 431)
        btnback.Name = "btnback"
        btnback.Size = New Size(104, 42)
        btnback.TabIndex = 7
        btnback.Text = "Kembali"
        btnback.UseVisualStyleBackColor = True
        ' 
        ' login
        ' 
        AutoScaleDimensions = New SizeF(8F, 20F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(505, 733)
        Controls.Add(btnback)
        Controls.Add(btnlogin)
        Controls.Add(txtpassword2)
        Controls.Add(Label3)
        Controls.Add(txtusername2)
        Controls.Add(Label2)
        Controls.Add(Label1)
        Name = "login"
        StartPosition = FormStartPosition.CenterScreen
        Text = "LOGIN FORM"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtusername2 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtpassword2 As TextBox
    Friend WithEvents btnlogin As Button
    Friend WithEvents btnback As Button
End Class
