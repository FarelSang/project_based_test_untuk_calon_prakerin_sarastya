﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class register
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Label1 = New Label()
        Label2 = New Label()
        btnregister = New Button()
        txtnama = New TextBox()
        Label3 = New Label()
        combojurusan = New ComboBox()
        Label4 = New Label()
        txtusername = New TextBox()
        txtpassword = New TextBox()
        Label5 = New Label()
        tologinbtn = New Button()
        Label6 = New Label()
        comboekstra = New ComboBox()
        SuspendLayout()
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Font = New Font("Stencil", 19.8000011F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        Label1.Location = New Point(73, 18)
        Label1.Name = "Label1"
        Label1.Size = New Size(354, 40)
        Label1.TabIndex = 0
        Label1.Text = "Form Pendaftaran"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label2.Location = New Point(44, 96)
        Label2.Name = "Label2"
        Label2.Size = New Size(65, 28)
        Label2.TabIndex = 1
        Label2.Text = "Nama"
        ' 
        ' btnregister
        ' 
        btnregister.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        btnregister.Location = New Point(209, 576)
        btnregister.Name = "btnregister"
        btnregister.Size = New Size(106, 43)
        btnregister.TabIndex = 2
        btnregister.Text = "Register"
        btnregister.UseVisualStyleBackColor = True
        ' 
        ' txtnama
        ' 
        txtnama.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtnama.Location = New Point(44, 128)
        txtnama.Name = "txtnama"
        txtnama.Size = New Size(349, 34)
        txtnama.TabIndex = 3
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label3.Location = New Point(44, 174)
        Label3.Name = "Label3"
        Label3.Size = New Size(82, 28)
        Label3.TabIndex = 4
        Label3.Text = "Jurusan"
        ' 
        ' combojurusan
        ' 
        combojurusan.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        combojurusan.FormattingEnabled = True
        combojurusan.Items.AddRange(New Object() {"RPL", "DKV", "TKJ", "BP"})
        combojurusan.Location = New Point(44, 205)
        combojurusan.Name = "combojurusan"
        combojurusan.Size = New Size(209, 36)
        combojurusan.TabIndex = 5
        combojurusan.Text = "Pilih Jurusan"
        ' 
        ' Label4
        ' 
        Label4.AutoSize = True
        Label4.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label4.Location = New Point(42, 368)
        Label4.Name = "Label4"
        Label4.Size = New Size(104, 28)
        Label4.TabIndex = 6
        Label4.Text = "Username"
        ' 
        ' txtusername
        ' 
        txtusername.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtusername.Location = New Point(44, 402)
        txtusername.Name = "txtusername"
        txtusername.Size = New Size(207, 34)
        txtusername.TabIndex = 7
        ' 
        ' txtpassword
        ' 
        txtpassword.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtpassword.Location = New Point(44, 485)
        txtpassword.MaxLength = 5
        txtpassword.Name = "txtpassword"
        txtpassword.PasswordChar = "*"c
        txtpassword.Size = New Size(207, 34)
        txtpassword.TabIndex = 8
        ' 
        ' Label5
        ' 
        Label5.AutoSize = True
        Label5.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label5.Location = New Point(42, 451)
        Label5.Name = "Label5"
        Label5.Size = New Size(44, 28)
        Label5.TabIndex = 9
        Label5.Text = "NIS"
        ' 
        ' tologinbtn
        ' 
        tologinbtn.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        tologinbtn.Location = New Point(150, 625)
        tologinbtn.Name = "tologinbtn"
        tologinbtn.Size = New Size(229, 36)
        tologinbtn.TabIndex = 10
        tologinbtn.Text = "Sudah Punya Akun?"
        tologinbtn.UseVisualStyleBackColor = True
        ' 
        ' Label6
        ' 
        Label6.AutoSize = True
        Label6.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        Label6.Location = New Point(44, 269)
        Label6.Name = "Label6"
        Label6.Size = New Size(82, 28)
        Label6.TabIndex = 11
        Label6.Text = "Jurusan"
        ' 
        ' comboekstra
        ' 
        comboekstra.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        comboekstra.FormattingEnabled = True
        comboekstra.Items.AddRange(New Object() {"BOLA VOLY", "BAND", "BOLA BASKET", "KARYA ILMIAH REMAJA", "PASKIBRA", "BANJARI", "FOTOGRAFI"})
        comboekstra.Location = New Point(44, 311)
        comboekstra.Name = "comboekstra"
        comboekstra.Size = New Size(209, 36)
        comboekstra.TabIndex = 12
        comboekstra.Text = "Pilih Ekstrakulikuler"
        ' 
        ' register
        ' 
        AutoScaleDimensions = New SizeF(8F, 20F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(505, 733)
        Controls.Add(comboekstra)
        Controls.Add(Label6)
        Controls.Add(tologinbtn)
        Controls.Add(Label5)
        Controls.Add(txtpassword)
        Controls.Add(txtusername)
        Controls.Add(Label4)
        Controls.Add(combojurusan)
        Controls.Add(Label3)
        Controls.Add(txtnama)
        Controls.Add(btnregister)
        Controls.Add(Label2)
        Controls.Add(Label1)
        Name = "register"
        StartPosition = FormStartPosition.CenterScreen
        Text = "FORM PENDAFTARAN"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnregister As Button
    Friend WithEvents txtnama As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents combojurusan As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtusername As TextBox
    Friend WithEvents txtpassword As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents tologinbtn As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents comboekstra As ComboBox

End Class
