﻿Imports System.Data.Odbc
Public Class edit
    Public conn As OdbcConnection
    Public da As OdbcDataAdapter
    Public ds As DataSet
    Public cmd As OdbcCommand
    Public dr As OdbcDataReader
    Public mydb As String
    Sub KoneksiDatabase()
        Try
            mydb = "Driver={PostgreSQL ANSI(x64)};Database=testsarasatya;port=5432;Server=localhost;uid=postgres;password=admin"
            conn = New OdbcConnection(mydb)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox("gagal", vbCritical, "gagal")
        End Try
    End Sub
    Private Sub edit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call awal()
    End Sub

    Sub awal()
        Call KoneksiDatabase()
    End Sub

    Private Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        profile.Show()
        Me.Close()
    End Sub

    Private Sub btnedit_Click(sender As Object, e As EventArgs) Handles btnedit.Click
        Dim nama As String
        Dim jurusan As String
        Dim username As String
        Dim password As String
        Dim ekstra As String

        nama = txtnama.Text
        jurusan = combojurusan.SelectedItem
        ekstra = lblekstra.Text
        username = txtusername.Text
        password = txtpassword.Text

        Call KoneksiDatabase()
        Dim inputdata As String = "UPDATE profile SET nama = '" & nama & "', jurusan = '" & jurusan & "', username = '" & username & "', pass = '" & password & "', ekskul = '" & ekstra & "' WHERE username = '" & lbluser.Text & "'"
        cmd = New OdbcCommand(inputdata, conn)
        cmd.ExecuteNonQuery()
        MessageBox.Show("Berhasil Edit", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        Dim login As String = "SELECT nama, jurusan, username, pass, ekskul FROM profile where username='" & txtusername.Text & "'"
        cmd = New OdbcCommand(login, conn)
        Dim dr As OdbcDataReader = cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read()
                Dim n As Object = dr("nama")
                Dim j As Object = dr("jurusan")
                Dim u As Object = dr("username")
                Dim p As Object = dr("pass")
                Dim eks As Object = dr("ekskul")

                profile.lblnama.Text = n
                profile.lbljurusan.Text = j
                profile.lblekstra.Text = eks
                profile.lblusername.Text = u
                profile.lblpassword.Text = p
            End While
        End If
        profile.Show()
        Me.Close()
    End Sub
End Class