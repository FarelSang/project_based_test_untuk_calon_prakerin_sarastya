﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Label1 = New Label()
        lbluser = New Label()
        Label5 = New Label()
        Label4 = New Label()
        Label3 = New Label()
        Label2 = New Label()
        txtpassword = New TextBox()
        txtusername = New TextBox()
        combojurusan = New ComboBox()
        txtnama = New TextBox()
        btnedit = New Button()
        btnback = New Button()
        Label6 = New Label()
        lblekstra = New TextBox()
        SuspendLayout()
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Font = New Font("Stencil", 19.8000011F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        Label1.Location = New Point(127, 20)
        Label1.Name = "Label1"
        Label1.Size = New Size(239, 40)
        Label1.TabIndex = 2
        Label1.Text = "EDIT PROFILE"
        ' 
        ' lbluser
        ' 
        lbluser.AutoSize = True
        lbluser.Location = New Point(14, 21)
        lbluser.Name = "lbluser"
        lbluser.Size = New Size(53, 20)
        lbluser.TabIndex = 3
        lbluser.Text = "Label2"
        lbluser.Visible = False
        ' 
        ' Label5
        ' 
        Label5.AutoSize = True
        Label5.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label5.Location = New Point(64, 502)
        Label5.Name = "Label5"
        Label5.Size = New Size(45, 28)
        Label5.TabIndex = 15
        Label5.Text = "NIS"
        ' 
        ' Label4
        ' 
        Label4.AutoSize = True
        Label4.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label4.Location = New Point(64, 411)
        Label4.Name = "Label4"
        Label4.Size = New Size(106, 28)
        Label4.TabIndex = 14
        Label4.Text = "Username"
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label3.Location = New Point(66, 193)
        Label3.Name = "Label3"
        Label3.Size = New Size(85, 28)
        Label3.TabIndex = 13
        Label3.Text = "Jurusan"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label2.Location = New Point(66, 98)
        Label2.Name = "Label2"
        Label2.Size = New Size(68, 28)
        Label2.TabIndex = 11
        Label2.Text = "Nama"
        ' 
        ' txtpassword
        ' 
        txtpassword.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtpassword.Location = New Point(78, 554)
        txtpassword.MaxLength = 5
        txtpassword.Name = "txtpassword"
        txtpassword.PasswordChar = "*"c
        txtpassword.Size = New Size(207, 34)
        txtpassword.TabIndex = 19
        ' 
        ' txtusername
        ' 
        txtusername.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtusername.Location = New Point(78, 464)
        txtusername.Name = "txtusername"
        txtusername.Size = New Size(207, 34)
        txtusername.TabIndex = 18
        ' 
        ' combojurusan
        ' 
        combojurusan.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        combojurusan.FormattingEnabled = True
        combojurusan.Items.AddRange(New Object() {"RPL", "DKV", "TKJ", "BP"})
        combojurusan.Location = New Point(78, 244)
        combojurusan.Name = "combojurusan"
        combojurusan.Size = New Size(209, 36)
        combojurusan.TabIndex = 17
        combojurusan.Text = "Pilih Jurusan"
        ' 
        ' txtnama
        ' 
        txtnama.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        txtnama.Location = New Point(78, 147)
        txtnama.Name = "txtnama"
        txtnama.Size = New Size(349, 34)
        txtnama.TabIndex = 16
        ' 
        ' btnedit
        ' 
        btnedit.Location = New Point(270, 619)
        btnedit.Name = "btnedit"
        btnedit.Size = New Size(94, 29)
        btnedit.TabIndex = 21
        btnedit.Text = "EDIT"
        btnedit.UseVisualStyleBackColor = True
        ' 
        ' btnback
        ' 
        btnback.Location = New Point(138, 619)
        btnback.Name = "btnback"
        btnback.Size = New Size(94, 29)
        btnback.TabIndex = 20
        btnback.Text = "KEMBALI"
        btnback.UseVisualStyleBackColor = True
        ' 
        ' Label6
        ' 
        Label6.AutoSize = True
        Label6.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label6.Location = New Point(64, 305)
        Label6.Name = "Label6"
        Label6.Size = New Size(155, 28)
        Label6.TabIndex = 24
        Label6.Text = "Ekstrakurikuler"
        ' 
        ' lblekstra
        ' 
        lblekstra.Enabled = False
        lblekstra.Font = New Font("Segoe UI Semibold", 12F, FontStyle.Bold)
        lblekstra.Location = New Point(78, 362)
        lblekstra.Name = "lblekstra"
        lblekstra.Size = New Size(207, 34)
        lblekstra.TabIndex = 25
        ' 
        ' edit
        ' 
        AutoScaleDimensions = New SizeF(8F, 20F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(505, 733)
        Controls.Add(lblekstra)
        Controls.Add(Label6)
        Controls.Add(btnedit)
        Controls.Add(btnback)
        Controls.Add(txtpassword)
        Controls.Add(txtusername)
        Controls.Add(combojurusan)
        Controls.Add(txtnama)
        Controls.Add(Label5)
        Controls.Add(Label4)
        Controls.Add(Label3)
        Controls.Add(Label2)
        Controls.Add(lbluser)
        Controls.Add(Label1)
        Name = "edit"
        StartPosition = FormStartPosition.CenterScreen
        Text = "edit"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents lbluser As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtpassword As TextBox
    Friend WithEvents txtusername As TextBox
    Friend WithEvents combojurusan As ComboBox
    Friend WithEvents txtnama As TextBox
    Friend WithEvents btnedit As Button
    Friend WithEvents btnback As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents lblekstra As TextBox
End Class
