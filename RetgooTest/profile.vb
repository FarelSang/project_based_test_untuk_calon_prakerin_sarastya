﻿Imports System.Data.Odbc
Public Class profile
    Public conn As OdbcConnection
    Public da As OdbcDataAdapter
    Public ds As DataSet
    Public cmd As OdbcCommand
    Public dr As OdbcDataReader
    Public mydb As String
    Sub KoneksiDatabase()
        Try
            mydb = "Driver={PostgreSQL ANSI(x64)};Database=testsarasatya;port=5432;Server=localhost;uid=postgres;password=admin"
            conn = New OdbcConnection(mydb)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox("gagal", vbCritical, "gagal")
        End Try
    End Sub
    Private Sub profile_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call awal()
    End Sub

    Private Sub btnlogout_Click(sender As Object, e As EventArgs) Handles btnlogout.Click
        login.Show()
        Me.Close()
    End Sub

    Sub awal()
        Call KoneksiDatabase()
        Dim login As String = "SELECT nama, jurusan, username, pass, ekskul FROM profile where username='" & lbluser.Text & "'"
        cmd = New OdbcCommand(login, conn)
        Dim dr As OdbcDataReader = cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read()
                Dim nama As Object = dr("nama")
                Dim jurusan As Object = dr("jurusan")
                Dim username As Object = dr("username")
                Dim password As Object = dr("pass")
                Dim ekstra As Object = dr("ekskul")

                lblnama.Text = nama
                lbljurusan.Text = jurusan
                lblekstra.Text = ekstra
                lblusername.Text = username
                lblpassword.Text = password
            End While
        End If

    End Sub

    Private Sub btnedit_Click(sender As Object, e As EventArgs) Handles btnedit.Click
        Dim ambil As String = "SELECT nama, jurusan, username, pass, ekskul FROM profile where username='" & lbluser.Text & "'"
        cmd = New OdbcCommand(ambil, conn)
        Dim dr As OdbcDataReader = cmd.ExecuteReader()
        If dr.HasRows Then
            While dr.Read()
                Dim nama As String = dr("nama")
                Dim jurusan As String = dr("jurusan")
                Dim username As String = dr("username")
                Dim password As String = dr("pass")
                Dim ekstra As Object = dr("ekskul")

                edit.txtnama.Text = nama
                edit.combojurusan.SelectedItem = jurusan
                edit.lblekstra.Text = ekstra
                edit.txtusername.Text = username
                edit.txtpassword.Text = password
            End While
        End If
        edit.Show()
        Me.Close()
        edit.lbluser.Text = lbluser.Text
    End Sub

End Class