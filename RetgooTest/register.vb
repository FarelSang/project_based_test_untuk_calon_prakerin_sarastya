﻿Imports System.Data.Odbc
Public Class register
    Public conn As OdbcConnection
    Public da As OdbcDataAdapter
    Public ds As DataSet
    Public cmd As OdbcCommand
    Public dr As OdbcDataReader
    Public mydb As String
    Sub KoneksiDatabase()
        Try
            mydb = "Driver={PostgreSQL ANSI(x64)};Database=testsarasatya;port=5432;Server=localhost;uid=postgres;password=admin"
            conn = New OdbcConnection(mydb)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox("gagal", vbCritical, "gagal")
        End Try
    End Sub
    Private Sub btnregister_Click_1(sender As Object, e As EventArgs) Handles btnregister.Click
        Dim nama As String
        Dim jurusan As String
        Dim username As String
        Dim password As String
        Dim ekstra As String

        nama = txtnama.Text
        jurusan = combojurusan.SelectedItem
        username = txtusername.Text
        password = txtpassword.Text
        ekstra = comboekstra.SelectedItem

        If txtnama.Text.Trim() = "" Or combojurusan.SelectedItem = "" Or txtusername.Text = "" Or txtpassword.Text = "" Or comboekstra.SelectedItem = "" Then
            MessageBox.Show("Data Tidak Boleh Kosong", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Call KoneksiDatabase()
            Dim inputdata As String = "INSERT INTO profile values ('" & nama & "','" & jurusan & "','" & username & "','" & password & "', '" & ekstra & "')"
            cmd = New OdbcCommand(inputdata, conn)
            cmd.ExecuteNonQuery()
            MessageBox.Show("Berhasil Register", "Register", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            login.Show()
            Me.Close()
        End If
    End Sub

    Private Sub tologinbtn_Click(sender As Object, e As EventArgs) Handles tologinbtn.Click
        login.Show()
        Me.Close()
    End Sub

    Private Sub register_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        awal()
    End Sub

    Sub awal()
        Call KoneksiDatabase()
        txtnama.Text = ""
        combojurusan.SelectedText = ""
        comboekstra.SelectedItem = ""
        txtusername.Text = ""
        txtpassword.Text = ""
        btnregister.Text = "REGISTER"
        tologinbtn.Text = "SUDAH PUNYA AKUN?"
    End Sub

End Class
