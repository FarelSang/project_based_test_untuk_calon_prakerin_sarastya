﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class profile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Label1 = New Label()
        Label2 = New Label()
        lblnama = New Label()
        Label3 = New Label()
        Label4 = New Label()
        Label5 = New Label()
        lbljurusan = New Label()
        lblusername = New Label()
        lblpassword = New Label()
        btnlogout = New Button()
        btnedit = New Button()
        lbluser = New Label()
        Label6 = New Label()
        lblekstra = New Label()
        SuspendLayout()
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Font = New Font("Stencil", 19.8000011F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        Label1.Location = New Point(127, 26)
        Label1.Name = "Label1"
        Label1.Size = New Size(250, 40)
        Label1.TabIndex = 2
        Label1.Text = "User Profile"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label2.Location = New Point(66, 98)
        Label2.Name = "Label2"
        Label2.Size = New Size(68, 28)
        Label2.TabIndex = 3
        Label2.Text = "Nama"
        ' 
        ' lblnama
        ' 
        lblnama.BackColor = SystemColors.ControlLight
        lblnama.BorderStyle = BorderStyle.Fixed3D
        lblnama.Font = New Font("Segoe UI Semibold", 10.8F, FontStyle.Bold)
        lblnama.Location = New Point(127, 138)
        lblnama.Name = "lblnama"
        lblnama.Size = New Size(220, 45)
        lblnama.TabIndex = 4
        lblnama.Text = "Label3"
        lblnama.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label3.Location = New Point(66, 193)
        Label3.Name = "Label3"
        Label3.Size = New Size(85, 28)
        Label3.TabIndex = 5
        Label3.Text = "Jurusan"
        ' 
        ' Label4
        ' 
        Label4.AutoSize = True
        Label4.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label4.Location = New Point(66, 405)
        Label4.Name = "Label4"
        Label4.Size = New Size(106, 28)
        Label4.TabIndex = 6
        Label4.Text = "Username"
        ' 
        ' Label5
        ' 
        Label5.AutoSize = True
        Label5.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label5.Location = New Point(66, 498)
        Label5.Name = "Label5"
        Label5.Size = New Size(45, 28)
        Label5.TabIndex = 7
        Label5.Text = "NIS"
        ' 
        ' lbljurusan
        ' 
        lbljurusan.BackColor = SystemColors.ControlLight
        lbljurusan.BorderStyle = BorderStyle.Fixed3D
        lbljurusan.Font = New Font("Segoe UI Semibold", 10.8F, FontStyle.Bold)
        lbljurusan.Location = New Point(127, 239)
        lbljurusan.Name = "lbljurusan"
        lbljurusan.Size = New Size(220, 45)
        lbljurusan.TabIndex = 8
        lbljurusan.Text = "Label6"
        lbljurusan.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblusername
        ' 
        lblusername.BackColor = SystemColors.ControlLight
        lblusername.BorderStyle = BorderStyle.Fixed3D
        lblusername.Font = New Font("Segoe UI Semibold", 10.8F, FontStyle.Bold)
        lblusername.Location = New Point(127, 447)
        lblusername.Name = "lblusername"
        lblusername.Size = New Size(220, 45)
        lblusername.TabIndex = 9
        lblusername.Text = "Label6"
        lblusername.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblpassword
        ' 
        lblpassword.BackColor = SystemColors.ControlLight
        lblpassword.BorderStyle = BorderStyle.Fixed3D
        lblpassword.Font = New Font("Segoe UI Semibold", 10.8F, FontStyle.Bold)
        lblpassword.Location = New Point(127, 543)
        lblpassword.Name = "lblpassword"
        lblpassword.Size = New Size(220, 45)
        lblpassword.TabIndex = 10
        lblpassword.Text = "Label6"
        lblpassword.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' btnlogout
        ' 
        btnlogout.Location = New Point(151, 625)
        btnlogout.Name = "btnlogout"
        btnlogout.Size = New Size(94, 29)
        btnlogout.TabIndex = 11
        btnlogout.Text = "LOGOUT"
        btnlogout.UseVisualStyleBackColor = True
        ' 
        ' btnedit
        ' 
        btnedit.Location = New Point(283, 625)
        btnedit.Name = "btnedit"
        btnedit.Size = New Size(94, 29)
        btnedit.TabIndex = 12
        btnedit.Text = "EDIT"
        btnedit.UseVisualStyleBackColor = True
        ' 
        ' lbluser
        ' 
        lbluser.AutoSize = True
        lbluser.Location = New Point(9, 9)
        lbluser.Name = "lbluser"
        lbluser.Size = New Size(53, 20)
        lbluser.TabIndex = 13
        lbluser.Text = "Label6"
        lbluser.Visible = False
        ' 
        ' Label6
        ' 
        Label6.AutoSize = True
        Label6.Font = New Font("Segoe UI", 12F, FontStyle.Bold)
        Label6.Location = New Point(66, 298)
        Label6.Name = "Label6"
        Label6.Size = New Size(155, 28)
        Label6.TabIndex = 14
        Label6.Text = "Ekstrakurikuler"
        ' 
        ' lblekstra
        ' 
        lblekstra.BackColor = SystemColors.ControlLight
        lblekstra.BorderStyle = BorderStyle.Fixed3D
        lblekstra.Font = New Font("Segoe UI Semibold", 10.8F, FontStyle.Bold)
        lblekstra.Location = New Point(127, 341)
        lblekstra.Name = "lblekstra"
        lblekstra.Size = New Size(220, 45)
        lblekstra.TabIndex = 15
        lblekstra.Text = "Label6"
        lblekstra.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' profile
        ' 
        AutoScaleDimensions = New SizeF(8F, 20F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(505, 733)
        Controls.Add(lblekstra)
        Controls.Add(Label6)
        Controls.Add(lbluser)
        Controls.Add(btnedit)
        Controls.Add(btnlogout)
        Controls.Add(lblpassword)
        Controls.Add(lblusername)
        Controls.Add(lbljurusan)
        Controls.Add(Label5)
        Controls.Add(Label4)
        Controls.Add(Label3)
        Controls.Add(lblnama)
        Controls.Add(Label2)
        Controls.Add(Label1)
        Name = "profile"
        StartPosition = FormStartPosition.CenterScreen
        Text = "USER PROFILE"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblnama As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lbljurusan As Label
    Friend WithEvents lblusername As Label
    Friend WithEvents lblpassword As Label
    Friend WithEvents btnlogout As Button
    Friend WithEvents btnedit As Button
    Friend WithEvents lbluser As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lblekstra As Label
End Class
