﻿Imports System.Data.Odbc
Public Class login
    Public conn As OdbcConnection
    Public da As OdbcDataAdapter
    Public ds As DataSet
    Public cmd As OdbcCommand
    Public dr As OdbcDataReader
    Public mydb As String
    Sub KoneksiDatabase()
        Try
            mydb = "Driver={PostgreSQL ANSI(x64)};Database=testsarasatya;port=5432;Server=localhost;uid=postgres;password=admin"
            conn = New OdbcConnection(mydb)
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox("gagal", vbCritical, "gagal")
        End Try
    End Sub
    Private Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        register.Show()
        Me.Close()
        register.txtnama.Text = ""
        register.combojurusan.SelectedItem = ""
        register.comboekstra.SelectedItem = ""
        register.txtusername.Text = ""
        register.txtpassword.Text = ""

    End Sub

    Private Sub btnlogin_Click(sender As Object, e As EventArgs) Handles btnlogin.Click
        If txtusername2.Text.Trim() = "" Or txtpassword2.Text.Trim() = "" Then
            MessageBox.Show("Username Atau Password Tidak Boleh Kosong", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Call KoneksiDatabase()
            Dim login As String = "SELECT * FROM profile where username='" & txtusername2.Text & "'AND pass='" & txtpassword2.Text & "'"
            cmd = New OdbcCommand(login, conn)
            dr = cmd.ExecuteReader
            dr.Read()
            If dr.HasRows Then
                MessageBox.Show("Berhasil Login", "Login", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                profile.lbluser.Text = txtusername2.Text
                profile.Show()
                Me.Close()
            Else
                MessageBox.Show("Username atau Password salah", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtusername2.Text = ""
                txtpassword2.Text = ""
            End If
        End If


    End Sub

    Private Sub login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        awal()
    End Sub

    Sub awal()
        Call KoneksiDatabase()
        txtusername2.Text = ""
        txtpassword2.Text = ""
        btnback.Text = "REGISTER"
        btnlogin.Text = "LOGIN"
    End Sub


End Class